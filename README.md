# Live coding

## Live Coding I:
[Primeira tentativa de live coding de R.](https://youtu.be/I4ErDOwdXrk)

Tentamos reproduzir [a analise feita](https://twitter.com/NarceliodeSa/status/1289745605338816513?s=20) pelo [Narcélio de Sá](https://twitter.com/NarceliodeSa) com os dados de COVID-19. Dados disponibilizados pelo próprio Narcélio.


Estiveram presentes: [Kyle Felipe](https://twitter.com/kylefelipe), [Felipe Sodré](https://twitter.com/FelipeSMBarros) e o **salvador da live: [Mauricio Vancine](https://twitter.com/mauriciovancine)** :trophy:.

### Observações:
Pesquisando sobre a técnica usada, cheguei à informação de quea mesa é chamada de `centrografia` ([Olaya](https://volaya.github.io/libro-sig/chapters/Estadistica_espacial.html), [Manuel Gimond](https://mgimond.github.io/Spatial/point-pattern-analysis.html). Segundo [Victor Olaya]():
> El equivalente espacial de las medidas de tendencia central como el momento de primer orden (media) o la mediana, así como de las de dispersión tales como el momento de segundo orden (desviación típica).

![](./img/covid.png)

![](./img/covid.gif)

## Live Coding II:
[Live coding II](https://www.youtube.com/watch?v=5BipR4L-n1Q) foi realizada com Mauricio Vancine, Felipe Barros e João giovanelli.

Fizemos uma análise de sensoriamento remoto para identificaçãod e queimada (NBR) e severidade (DNBR);

## Live Coding III:
[Live coding III](https://www.youtube.com/watch?v=CQw8n_WpTMI) foi realizada com a presença do professor Gustavo Baptista, professor da UNB e produtor do podcast "O fantastico mundo do sensoriamento remoto" e Felipe Barros.

Nessa live tiramos duvidas e vimos alternativas de análise como o RDNBR;  

