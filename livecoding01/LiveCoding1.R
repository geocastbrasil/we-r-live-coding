# carregando pacotes
library(sf)
library(tmap)
library(geobr)
library(dplyr)

# Carregando os dados

# covid
casos <- read_sf("./data/dados_covid_br.geojson", as_tibble = TRUE)
casos
names(casos)
glimpse(casos)

# estados
uf <- read_state()

# plot dados carregados
tm_shape(uf) +
  tm_borders() +
tm_shape(casos) + 
  tm_dots(size = .5)

# Media
class(casos)
casos %>% st_coordinates() %>% as_tibble() %>% select(X)

# casos ponderados
wcasos <- casos %>% 
  filter( new_confirmed != 0) %>%
  mutate(
    longitude = st_coordinates(.) %>% as_tibble() %>% select(X),
    latitude = st_coordinates(.) %>% as_tibble() %>% select(Y)) %>% 
  group_by(date) %>% 
  summarise(
    meanWlon = weighted.mean(longitude$X, new_confirmed),
    meanWlat = weighted.mean(latitude$Y,  new_confirmed),
    totalCasos = sum(new_confirmed)) %>% 
    st_drop_geometry(.)
wcasos

# transformando o casos ponderado em dado espacial
wcasos <- st_as_sf(wcasos, coords = c("meanWlon", "meanWlat"), crs = 4326)
wcasos

# tentativa animação (fracassada)
# map <- tm_shape(uf, bbox = uf)+
#   tm_borders()+
#   tm_shape(wcasos) +
#   tm_dots("totalCasos", size = .3, palette = "viridis") +
#   tm_facets("date", ncol = 1, nrow = 1)
# tmap_animation(map, "./img/covid.gif")

# mapa - elaborado pós live
map <- tm_shape(uf, bbox = uf)+
    tm_borders()+
    tm_shape(wcasos) +
    tm_dots("totalCasos", size = .3, palette = "viridis") + 
  tm_grid(lwd = 0) + 
  tm_layout(main.title = "Mapa centrográfico",
            main.title.position =  c("right", "TOP"),
            title = "COVID-19 por estado",
            title.position = c("right", "TOP"))
map
tmap_save(map, "./img/covid.png")
  